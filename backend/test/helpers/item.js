import { queryAsync } from '../../src/db';
import { createItem } from '../../src/models/item';

export async function resetItemsTable() {
  return queryAsync('TRUNCATE TABLE item');
}

export async function createItems(names) {
  return Promise.all(names.map(createItem));
}
