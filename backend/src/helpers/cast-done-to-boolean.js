/**
 * Convertit la propriété done d'un objet item en booléen.
 * L'objet item est un enregistrement de la table item.
 * Il a la forme { id: 1, name: "Écrire des tests", done: 0, createdAt: "2021-09-01T12:00:00.000Z", updatedAt: "2021-09-01T12:00:00.000Z" }.
 * L'attribut done est un entier 0 ou 1, et on veut le convertir en false ou true.
 *
 * @param {object} item objet représentant un enregistrement de la table item
 * @returns
 */
export default function castDoneToBoolean(item) {
  return {
    ...item,
    done: Boolean(item.done),
  };
}
