import { useEffect } from 'react';
import PropTypes from 'prop-types';

import './DismissibleAlert.css';

export default function DismissibleAlert({ alert, setAlert }) {
  useEffect(() => {
    const timer = setTimeout(() => {
      setAlert(null);
    }, 5000);
    return () => clearTimeout(timer);
  }, [alert, setAlert]);

  // Div avec la meme taille que s'il y avait un message d'alerte
  if (!alert) {
    return <div className="DismissibleAlert" />;
  }
  return (
    <div className={`DismissibleAlert DismissibleAlert-${alert.status}`}>
      <span>{alert.message}</span>
      <button onClick={() => setAlert(null)}>X</button>
    </div>
  );
}

DismissibleAlert.propTypes = {
  alert: PropTypes.shape({
    message: PropTypes.string,
    status: PropTypes.string,
  }),
  setAlert: PropTypes.func.isRequired,
};
