import { useEffect, useState } from 'react';

import { readServerMessage } from './api';
import TodoList from './TodoList';

import './App.css';
import AppTitle from './components/AppTitle';
import Footer from './components/Footer';

function App() {
  const [message, setMessage] = useState('');
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await readServerMessage();
        setMessage(data.message);
      } catch (err) {
        setError(err);
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, []);

  if (loading) return <p>Loading...</p>;

  return (
    <div className="App">
      <div className="App-inner">
        <div className="container">
          <AppTitle title="Todo App" />
          <TodoList />
        </div>
      </div>
      <Footer message={message} error={error} />
    </div>
  );
}

export default App;
